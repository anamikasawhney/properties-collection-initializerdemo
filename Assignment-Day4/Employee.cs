﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Day4
{// property > wrapper around the private variables
    // properties shud be public
    // properties has 2 methods , getter & setter
    // getter is used to return value of the private variable to which it belongs to
    // setter is used to store value in the private variable to which it belongs
    // we shud keep the variables private , so that it shud be protected 
    // data hiding > 

    internal class Employee
    {
        int id;
        public int EmployeeId
        {
            get
            {
                return id;
            }
            set
            {
                if(value<=0)
                {
                    throw new Exception("Id cant be negtaive or 0");
                }
                id = value;
            }
        }

        string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        string dept;
        int exp;
        public void Get() { }
        public void Display() { }

    }
}
