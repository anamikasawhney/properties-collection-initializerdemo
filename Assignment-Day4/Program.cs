﻿using System.Runtime.CompilerServices;

namespace Assignment_Day4
{
    internal class Program
    {
        static void Main(string[] args)
        {
           Employee emp = new Employee();
            emp.EmployeeId = 10; // this will call set method
            Console.WriteLine(emp.EmployeeId); // this will call get method

        }
    }
}
