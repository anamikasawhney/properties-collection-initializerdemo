﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Day4
{
    internal class Student
    {
        public int Rn { get; set; }
        public string Name { get; set; }
        public string Dept { get; set; }
        public int Marks { get; set; }
        public DateTime Dob { get; set; }

        public void DisplayDetails()
        {
            Console.WriteLine($"Rn is {Rn}");
            Console.WriteLine($"Name is {Name}");
            Console.WriteLine($"Dept is {Dept}");
            Console.WriteLine($"DOB is {Dob.ToString("M/dd/yyy")}");
        }
        static void Main()
        {
            // Object Initializer
            //Student student = new Student();
            //student.Rn = 10;
            //student.Name = "ajay";

            Student student = new Student()
            {
                Rn = 1,
                Name = "Ajay",
                Dept = "IT",
                Marks = 90,
                Dob = Convert.ToDateTime("12/12/2009")
            };

            // List initializer
            //List<Student> students = new List<Student>();

            //Student student1 = new Student()
            //{
            //    Rn = 1,
            //    Name = "Ajay",
            //    Dept = "IT",
            //    Marks = 90,
            //    Dob = Convert.ToDateTime("12/12/2009")
            //};

            //Student student2 = new Student()
            //{
            //    Rn = 1,
            //    Name = "Ajay",
            //    Dept = "IT",
            //    Marks = 90,
            //    Dob = Convert.ToDateTime("12/12/2009")
            //};
            //students.Add(student1); 
            //students.Add(student2);


            List<Student> students = new List<Student>()
            {
                new Student() { Rn = 1, Name = "Deepak", Dept = "Elec", Dob = DateTime.Parse("12/01/2006"), Marks = 90 },

                new Student() { Rn = 2, Name = "Deepak", Dept = "Elec", Dob = DateTime.Parse("12/01/2006"), Marks = 90 },

                new Student() { Rn = 3, Name = "Deepak", Dept = "Elec", Dob = DateTime.Parse("12/01/2006"), Marks = 90 },

                new Student() { Rn = 4, Name = "Deepak", Dept = "Elec", Dob = DateTime.Parse("12/01/2006"), Marks = 90 }
            };
            foreach(Student temp in students)
            {
                temp.DisplayDetails();
            }

            Dictionary<int, Student> list = new Dictionary<int, Student>()
            {
                {1, new Student() {Rn = 1,Name="ajay", Dept="Ele"} },
                {2, new Student() {Rn = 1,Name="ajay", Dept="Ele"} }
            };
            if(list.ContainsKey(2))
            {
                list[2].DisplayDetails();
            }
        }
            }

        
        }
     