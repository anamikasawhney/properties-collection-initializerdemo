﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{// property > wrapper around the private variables
    // properties shud be public
    // properties has 2 methods , getter & setter
    // getter is used to return value of the private variable to which it belongs to
    // setter is used to store value in the private variable to which it belongs
    // we shud keep the variables private , so that it shud be protected 
    // data hiding > 

    internal class Employee
    {

        private int id;

        //public int Id
        //{
        //    get { return id; }
        //    set { id = value; }
        //}

        //private string name;

        //public string Name
        //{
        //    get { return name; }
        //    set { name = value; }
        //}
       
        public void Get() { }
        public void Display() { }

    }
}
